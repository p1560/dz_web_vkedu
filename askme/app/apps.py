from django.apps import AppConfig


def ready(self):
        import myapp.signals  # noqa

class AppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'app'

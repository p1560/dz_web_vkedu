from django import forms
from .models import *

class LoginForm(forms.Form):
        username = forms.CharField(max_length = 64, label="Username", required=True, widget=forms.TextInput())
        password = forms.CharField(max_length = 64, label="Password", required=True, widget=forms.PasswordInput())


class RegisterForm(forms.Form):
        email = forms.CharField(max_length = 64, label="Email", required=True, widget=forms.TextInput())
        username = forms.CharField(max_length = 64, label="Username", required=True, widget=forms.TextInput())
        password = forms.CharField(max_length = 64, label="Password", required=True, widget=forms.PasswordInput())
        confirm_password = forms.CharField(max_length = 64, label="Confirm password", required=True, widget=forms.PasswordInput())



class QuestionForm(forms.Form):
        TAG_CHOICES = Tag.objects.all()
        title = forms.CharField(max_length = 32, label = "Label", required = True, widget = forms.TextInput())
        body = forms.CharField(max_length = 4096, label = "Body", required = True, widget = forms.TextInput())

        tags = forms.MultipleChoiceField(choices = list([tag.name, tag.name] for tag in TAG_CHOICES), label = "tags", required = True, widget=forms.SelectMultiple)


class AnswerForm(forms.Form):
    answer = forms.CharField(max_length = 4096, label = "Answer", required = True, widget = forms.TextInput())

class SettingsForm(forms.Form):
        email = forms.CharField(max_length = 64, label="Email", required=False, widget=forms.TextInput())
        username = forms.CharField(max_length = 64, label="Username", required=False, widget=forms.TextInput())
        password = forms.CharField(max_length = 64, label="Password", required=False, widget=forms.PasswordInput())
        confirm_password = forms.CharField(max_length = 64, label="Confirm password", required=False, widget=forms.PasswordInput())
        avatar = forms.ImageField()
from django.contrib import admin

# Register your models here.

from .models import *

@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    list_display = ('label','date_of_creation')


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ('question','content')

admin.site.register(Tag)

@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'avatar')

admin.site.register(LikeDislikeAnswer)
admin.site.register(LikeDislikeQuestion)
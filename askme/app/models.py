from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db.models import Sum

# Create your models here.

class Meta:
    db_table = 'app'
    managed = True

class Tag(models.Model):
    name = models.TextField(max_length=60)

    def __str__(self):
        return self.name

class QuestionManager(models.Manager):
    def best_posts(self):
        return self.order_by('rating').reverse()
    def newest(self):
        return self.order_by('date_of_creation').reverse()
    def tag(self, tagname):
        return self.get_queryset().filter(tags__name=tagname)

class Question(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT)

    label = models.CharField(max_length = 128)

    date_of_creation = models.DateTimeField(auto_now_add=True)

    content = models.TextField(max_length = 4096)
    
    tags = models.ManyToManyField(Tag)

    objects = QuestionManager()

    rating = models.IntegerField(default = 0)

    def get_rating(self):
        if self.rating == 0:
            total_likes = LikeDislikeQuestion.objects.filter(question_id = self, reaction_type=1).aggregate(Sum('reaction_type'))['reaction_type__sum'] or 0
            total_dislikes = LikeDislikeQuestion.objects.filter(question_id = self, reaction_type=-1).aggregate(Sum('reaction_type'))['reaction_type__sum'] or 0
            rating = total_likes - total_dislikes
            self.rating = rating
            self.save()
            print(self.rating)
            return rating
        return self.rating
    


class Comment(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.PROTECT)

    content = models.TextField()

    date_of_creation = models.DateTimeField(auto_now_add=True)

    rating = models.IntegerField(default = 0)

    def get_rating(self):
        if self.rating == 0:
            total_likes = LikeDislikeAnswer.objects.filter(answer_id = self, reaction_type = 1).aggregate(Sum('reaction_type'))['reaction_type__sum'] or 0
            total_dislikes = LikeDislikeAnswer.objects.filter(answer_id = self, reaction_type = -1).aggregate(Sum('reaction_type'))['reaction_type__sum'] or 0
            rating = total_likes - total_dislikes
            self.rating = rating
            self.save()
            return rating
        return self.rating

class LikeDislikeQuestion(models.Model):
    question = models.ForeignKey(Question, on_delete = models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    reaction_type = models.SmallIntegerField(default = 0)

class LikeDislikeAnswer(models.Model):
    answer = models.ForeignKey(Comment, on_delete = models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    reaction_type = models.SmallIntegerField(default = 0)

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar = models.ImageField(upload_to = f'avatars/{user}', default = 'static/favicon.ico')
    text_status = models.CharField(max_length=100, blank=True, default="Happy")
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseNotFound
from .models import Question, Comment, Tag, User
from django.core.paginator import Paginator
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect
from django.urls import reverse
from .forms import *

def index(request):
    if ('search' in request.GET):
        search_text = request.GET['search']
        questions = Question.objects.filter(label__icontains = search_text)
    else:
        questions = Question.objects.newest().all()
    
    paginator = Paginator(questions, 20)
    tags = Tag.objects.all()[:5]
    users = User.objects.all()[:5]
    page = request.GET.get('page')
    questions = paginator.get_page(page)
    custom_range = questions.paginator.get_elided_page_range(questions.number, on_each_side=5, on_ends=2)
    return render(request, 'index.html', {'questions' : questions,
                                           'tags' : tags,
                                           'users' : users, 
                                           'ranges': custom_range,
                                           })  

def login_view(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        print(form.is_valid())
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(username=cd['username'], password=cd['password'])
            if user is not None:
                login(request, user)
                print("return 1")
                return redirect('index')
            else:
                return render(request, 'login.html', {'error': 'Invalid username or password'})
        else:
            return render(request, 'login.html', {'error': 'Invalid username or password'})
    else:
        form = LoginForm()  
        return render(request, 'login.html', {'form': form})  

def signup(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            if (User.objects.all().filter(email = cd['email']).exists()):
                return render(request, 'signup.html', {'error': 'User already exists'})
            elif (cd['password'] != cd['confirm_password']):
                return render(request, 'signup.html', {'error': 'Passwords dont match'})
            else:
                print("Created user")
                user = User(username = cd['username'],
                            email = cd['email'])
                user.set_password(cd['password'])
                user.save()
                profile = Profile(user = user)
                profile.save()
                login(request, user)
                return redirect('index')
    else:
        form = RegisterForm()  
        return render(request, 'signup.html', {'form': form})  

def question_view(request, question_id):
    question = get_object_or_404(Question, pk = question_id)
    comments = question.comment_set.all()
    tags = Tag.objects.all()[:10]
    users = User.objects.all()[:5]
    if request.user.is_authenticated:
        if request.method == "POST":
            if (request.user.is_superuser):
                question.delete()
                print("DEleted")
                return redirect('index')
            else:
                form = AnswerForm(request.POST)
                if form.is_valid():
                    cd = form.cleaned_data
                    answer = Comment(question = question, user = request.user, content = cd['answer'], rating = 0)
                    answer.save()
                    return redirect(request.path_info)
        
    return render(request, 'question.html',{'question': question, 
                                            'comments': comments, 
                                            'tags' : tags, 
                                            'users' : users,
                                           })

def settings(request):
    if not(request.user.is_authenticated):
        return redirect('login')
    else:
        if request.method == 'POST':
            form = SettingsForm(request.POST, request.FILES)
            print(request.FILES)
            if form.is_valid():
                cd = form.cleaned_data
                if cd['password'] != cd['confirm_password']:
                    return render(request, 'settings.html', {'error' : "Passwords don't match"})
                else:
                    if (cd['email']):
                        request.user.email = cd['email']
                    if (cd['username']):
                        request.user.username = cd['username']
                    if (cd['password'] and cd['confirm_password']):
                        request.user.set_password(cd['password'])
                    if (cd['avatar']):
                        request.user.avatar = request.FILES['avatar']
                    request.user.save()
                    return render(request, 'settings.html', {'success' : 'Info updated successfully!'})
            else:
                  return render(request, 'settings.html', {'error' : form.errors})
                
    return render(request, 'settings.html')


def hot(request):
    questions = Question.objects.best_posts().all()
    if ('search' in request.GET):
        search_text = request.GET['search']
        questions = questions.filter(label__icontains = search_text)
    paginator = Paginator(questions, 20)
    tags = Tag.objects.all()[:10]
    users = User.objects.all()[:5]
    page = request.GET.get('page')
    questions = paginator.get_page(page)
    custom_range = questions.paginator.get_elided_page_range(questions.number, on_each_side=5, on_ends=2)
    return render(request, 'hot.html', {'questions' : questions, 'tags' : tags, 'users' : users,
                                        'ranges': custom_range})      

def tag_view(request, tag_name):
    questions = Question.objects.tag(tag_name).all()
    if not questions:
        return HttpResponseNotFound("<h1>404 Not Found")
    paginator = Paginator(questions, 20)
    tags = Tag.objects.all()[:5]
    users = User.objects.all()[:5]
    page = request.GET.get('page')
    questions = paginator.get_page(page)
    custom_range = questions.paginator.get_elided_page_range(questions.number, on_each_side=5, on_ends=2)
    return render(request, 'tagged.html', {'questions' : questions, 'tags' : tags, 'users' : users,
                                        'ranges': custom_range, 'tagname' : tag_name})     


def logout_view(request):
    logout(request)
    return redirect('index')


def ask(request):
    tags = Tag.objects.all()
    if request.method == 'POST':
        form = QuestionForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            if (not(cd['tags'])):
               return render(request, 'ask.html', {'error': 'Select post tags'})
            if (len(cd['tags']) > 5):
                return render(request, 'ask.html', {'selectable_tags' : tags,
                                                    'error': 'Select less than 5 tags!'})
            else:
                print(cd['tags'])
                question = Question.objects.create(user = request.user,
                            label = cd['title'],
                            content = cd['body'], rating = 0)
                for tag_name in cd['tags']:
                    tag, created = Tag.objects.get_or_create(name = tag_name)
                    question.tags.add(tag)
                question.save()
                return redirect(reverse('question', args = [question.pk]))
        else:
            print(form['tags'])
            return render(request, 'ask.html', {'selectable_tags' : tags, 'error': form.errors})
    else:
        if not(request.user.is_authenticated):
            return redirect('login')
        form = QuestionForm()
        return render(request, 'ask.html', {'selectable_tags' : tags, 'form' : form})